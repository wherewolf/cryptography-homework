import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import java.lang.Comparable;
 
public class LetterFreq {
	public static int[] countLettersWithPunc(String filename) throws IOException{
		int[] freqs = new int[32];
		try(BufferedReader in = new BufferedReader(new FileReader(filename))){
			String line;
			while((line = in.readLine()) != null){
				line = line.toLowerCase();
				for(char ch:line.toCharArray()){
					if(Character.isLetter(ch)){
						freqs[ch - 'a']++;
					}
					else if(Character.isWhitespace(ch)){
						freqs[26]++;
					}
					else if(Character.compare(ch, '\'') == 0){
						freqs[27]++;
					}
					else if(Character.compare(ch, '-') == 0){
						freqs[28]++;
					}
					else if(Character.compare(ch, ',') == 0){
						freqs[29]++;
					}
					else if(Character.compare(ch, ';') == 0){
						freqs[30]++;
					}
					else if(Character.compare(ch, '.') == 0){
						freqs[31]++;
					}
				}
			}
		}
		return freqs;
	}

	// public static int[] countLettersNoPunc(String filename) throws IOException{
	// 	int[] freqs = new int[26];
	// 	try(BufferedReader in = new BufferedReader(new FileReader(filename))){
	// 		String line;
	// 		while((line = in.readLine()) != null){
	// 			//line = line.toLowerCase();
	// 			for(char ch:line.toCharArray()){
	// 				if(Character.isLetter(ch)){
	// 					freqs[ch - 'a']++;
	// 				}
	// 			}
	// 		}
	// 	}
	// 	return freqs;
	// }
 
	public static void main(String[] args) throws IOException{
		final char[] chars = new char[]{'a','b','c','d','e','f','g','h','i','j','k','l','m',
			'n','o','p','q','r','s','t','u','v','w','x','y','z',' ','\'','-',',',';','.'};
		// final char[] chars = new char[]{'a','b','c','d','e','f','g','h','i','j','k','l','m',
		// 	'n','o','p','q','r','s','t','u','v','w','x','y','z'};
		int [] freqs = countLettersWithPunc(args[0]);
		ArrayList<CharacterObject> toSort = new ArrayList<CharacterObject>();
		int total = 0;
		for(int x = 0; x < freqs.length; x++){
			total+= freqs[x];
			toSort.add(new CharacterObject(chars[x],freqs[x]));
		}
		Collections.sort(toSort, CharacterObject.COComparator);
		// for(CharacterObject co: toSort){
		// 	System.out.println(co);
	   //}
		System.out.println("total characters: "+total);
		System.out.printf("%s%12s%12s%12s%12s%12s%n","char","count","freq","char","count","freq");
		for(int i = 0; i < chars.length; i++){
			System.out.printf("%c%12d%14.2f%12c%12d%14.2f%n", chars[i],freqs[i],(freqs[i]*100.0/total),toSort.get(i).getC(), toSort.get(i).getI(), toSort.get(i).getI()*100.0/total);
		}
	}
}