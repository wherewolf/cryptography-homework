import java.util.Comparator;

public class CharacterObject {

        private char c;
        private int i;

        public CharacterObject(char c, int i) {
            this.c = c;
            this.i = i;
        }
        public int getI(){
            return this.i;
        }

        public char getC(){
            return this.c;
        }

        public static Comparator<CharacterObject> COComparator = new Comparator<CharacterObject>() {

        public int compare(CharacterObject co1, CharacterObject co2) {
           int coInt1 = co1.getI();
           int coInt2 = co2.getI();

           //descending order
           return Integer.compare(coInt2,coInt1);
        }
    };

    @Override
    public String toString() {
        return this.c + "         " + this.i;
    }
}